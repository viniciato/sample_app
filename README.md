# Ruby on Rails Tutorial sample application

This is the sample application for
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

## License

All source code in the [Ruby on Rails Tutorial](http://railstutorial.org/)
is available jointly under the MIT License and the Beerware License. See
[LICENSE.md](LICENSE.md) for details.

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

For more information, see the
[*Ruby on Rails Tutorial* book](http://www.railstutorial.org/book).

## Capitulo 4
4.2.2.1- city = “Guarapuava” | state = “PR”

4.2.2.2 - puts “Guarapuava, PR”

4.2.2.3 - puts “Guarapuava \t PR”

4.2.2.4 - Error


4.2.3.1 - 7

4.2.3.2 - Same!!

4.3.2.3 - True

4.3.2.4 - Rodando a primeira vez ele printa a frase, na segunda vez não printa pois não e a mesma coisa revertendo a frase.


4.2.4.1 - s.reverse == s

4.2.4.2 - ok

4.2.4.3 - ok


4.3.1.1 - a = “A man, a plan, a canal, Panama”.split(‘,’)

4.3.1.2 - s = a.join

4.3.1.3 - s = s.split(“ ”).join.downcase — True, is palindrome

4.3.1.4 - G - T


4.3.2.1 - 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32

4.3.2.2 - OK

4.3.2.3 - OK

4.3.2.4 - OK


4.3.3.1 - OK

4.3.3.2 - OK

4.3.3.3 - OK

4.3.3.4 - {"a"=>100, "b"=>300}


4.4.1.1 - a = Array.new([1,2,3,4,5,6,7,8,9,10])

4.4.1.2 - .

4.4.1.3 - OK


4.4.2.1 - Class, Module, Object, BasicObject

4.4.2.2 - OK


4.4.3.1 - TRUE, FALSE, TRUE

4.4.3.2 - OK

4.4.3.3 - NO


4.4.4.1 - ok

4.4.4.2 - Class, Module, Object, BasicObject


4.4.5.1 - OK

4.4.5.2 - OK

4.4.5.3 - OK


## Capitulo 6

# Section 6.1.1

1 - Rails uses a file called schema.rb in the db/ directory to keep track of the structure of the database (called the schema, hence the filename). Examine your local copy of db/schema.rb and compare its contents to the migration code in Listing 6.2.
```
  schema.rb possui todas as tabelas
```

2 - Most migrations (including all the ones in this tutorial) are reversible, which means we can “migrate down” and undo them with a single command, called db:rollback:
  $ rails db:rollback
After running this command, examine db/schema.rb to confirm that the rollback was successful. (See Box 3.1 for another technique useful for reversing migrations.) Under the hood, this command executes the drop_table command to remove the users table from the database. The reason this works is that the change method knows that drop_table is the inverse of create_table, which means that the rollback migration can be easily inferred. In the case of an irreversible migration, such as one to remove a database column, it is necessary to define separate up and down methods in place of the single change method. Read about migrations in the Rails Guides for more information.
```
  schema.rb ficou vazio após o rollback
```

3 - Re-run the migration by executing rails db:migrate again. Confirm that the contents of db/schema.rb have been restored.
```
  Foi restaurando após o comando.
```

# Section 6.1.2

1 - In a Rails console, use the technique from Section 4.4.4 to confirm that User.new is of class User and inherits from ApplicationRecord.
```
	user = User.new
	>> user.class.superclass
	=> ApplicationRecord(abstract)
```

2 - Confirm that ApplicationRecord inherits from ActiveRecord::Base.
```
	>> user.class.superclass.superclass
	=> ActiveRecord::Base
```

# Section 6.1.3

1 - Confirm that user.name and user.email are of class String.
```
	>> user.name.class
	=> String
```

2 - Of what class are the created_at and updated_at attributes?
```
	>> user.created_at.class
	=> ActiveSupport::TimeWithZone
```

# Section 6.1.4

1 - Find the user by name. Confirm that find_by_name works as well. (You will often encounter this older style of find_by in legacy Rails applications.)
```
>> User.find_by(name: "Michael Hartl")
  User Load (0.2ms)  SELECT  "users".* FROM "users" WHERE "users"."name" = ? LIMIT ?  [["name", "Michael Hartl"], ["LIMIT", 1]]
=> #<User id: 1, name: "Michael Hartl", email: "mhartl@example.com", created_at: "2017-04-14 19:36:47", updated_at: "2017-04-14 19:36:47">
```

2 - For most practical purposes, User.all acts like an array, but confirm that in fact it’s of class User::ActiveRecord_Relation.
```
	>> User.all.class
	=> User::ActiveRecord_Relation
```

3 - Confirm that you can find the length of User.all by passing it the length method (Section 4.2.3). Ruby’s ability to manipulate objects based on how they act rather than on their formal class type is called duck typing, based on the aphorism that “If it looks like a duck, and it quacks like a duck, it’s probably a duck.”
```
	>> User.all.length
  	User Load (0.1ms)  SELECT "users".* FROM "users"
	=> 2
```

# Section 6.1.5

1 - Update the user’s name using assignment and a call to save.
```
	>> user.name = "vinicius"
  => "vinicius"
  >> user.save
   (0.1ms)  SAVEPOINT active_record_1
  SQL (0.2ms)  UPDATE "users" SET "name" = ?, "updated_at" = ? WHERE "users"."id" = ?  [["name", "vinicius"], ["updated_at", 2017-04-14 19:44:11 UTC], ["id", 1]]
   (0.1ms)  RELEASE SAVEPOINT active_record_1
  => true
```

2 - Update the user’s email address using a call to update_attributes.
```
	>> user.update_attribute(:email, "vininadin@gmail.com")
   (0.1ms)  SAVEPOINT active_record_1
  SQL (0.1ms)  UPDATE "users" SET "email" = ?, "updated_at" = ? WHERE "users"."id" = ?  [["email", "vininadin@gmail.com"], ["updated_at", 2017-04-14 19:44:50 UTC], ["id", 1]]
   (0.1ms)  RELEASE SAVEPOINT active_record_1
  => true
```

3 - Confirm that you can change the magic columns directly by updating the created_at column using assignment and a save. Use the value 1.year.ago, which is a Rails way to create a timestamp one year before the present time.
```
	>> user.created_at = 1.year.ago
  => Thu, 14 Apr 2016 19:46:06 UTC +00:00
  >> user.save
   (0.1ms)  SAVEPOINT active_record_1
  SQL (0.2ms)  UPDATE "users" SET "created_at" = ?, "updated_at" = ? WHERE "users"."id" = ?  [["created_at", 2016-04-14 19:46:06 UTC], ["updated_at", 2017-04-14 19:46:09 UTC], ["id", 1]]
   (0.1ms)  RELEASE SAVEPOINT active_record_1
  => true
```

# Section 6.2.1

1 - In the console, confirm that a new user is currently valid.
```
Nao, porque o usuário foi criado em teste e foi deletado, não existe nenhum usuário no console
```

2 - Confirm that the user created in Section 6.1.3 is also valid.
```
Nao, porque quando eu sai do console o usuário foi deletado.
```

# Section 6.2.2

1 - Make a new user called u and confirm that it’s initially invalid. What are the full error messages?
```
  >> u = User.new
  => #<User id: nil, name: nil, email: nil, created_at: nil, updated_at: nil>
```

2 - Confirm that u.errors.messages is a hash of errors. How would you access just the email errors?
```
  >> u.errors.full_messages
  => ["Name can't be blank", "Email can't be blank"]
```

# Section 6.2.3

1 - Make a new user with too-long name and email and confirm that it’s not valid.
```
  >> name = "a" * 52
  => "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
  >> email = ("a" *255 + "gmail.com")
  => "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagmail.com"
  >> email.length
  => 264
  >> user = User.new(name: name, email: email)
  => #<User id: nil, name: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa...", email: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa...", created_at: nil, updated_at: nil>
  >> user.valid?
  => false
```

2 - What are the error messages generated by the length validation?
```
  >> user.errors.full_messages
  => ["Name is too long (maximum is 50 characters)", "Email is too long (maximum is 255 characters)"]
```

# Section 6.2.4

1 - By pasting in the valid addresses from Listing 6.18 and invalid addresses from Listing 6.19 into the test string area at Rubular, confirm that the regex from Listing 6.21 matches all of the valid addresses and none of the invalid ones.
```
  ?
```

2 - As noted above, the email regex in Listing 6.21 allows invalid email addresses with consecutive dots in the domain name, i.e., addresses of the form foo@bar..com. Add this address to the list of invalid addresses in Listing 6.19 to get a failing test, and then use the more complicated regex shown in Listing 6.23 to get the test to pass.
```
  ?
```

3 - Add foo@bar..com to the list of addresses at Rubular, and confirm that the regex shown in Listing 6.23 matches all the valid addresses and none of the invalid ones.
```
  ?
```

# Section 6.2.5

1 - Add a test for the email downcasing from Listing 6.32, as shown in Listing 6.33. This test uses the reload method for reloading a value from the database and the assert_equal method for testing equality. To verify that Listing 6.33 tests the right thing, comment out the before_save line to get to red, then uncomment it to get to green.
```
  OK
```

2 - By running the test suite, verify that the before_save callback can be written using the “bang” method email.downcase! to modify the email attribute directly, as shown in Listing 6.34.
```
  Sim, pode ser modificado desse jeito
```

# Section 6.3.2

1 - Confirm that a user with valid name and email still isn’t valid overall.
```
  >> user = User.new(name: "Example User", email: "user@example.com")
  => #<User id: nil, name: "Example User", email: "user@example.com", created_at: nil, updated_at: nil, password_digest: nil>
  >> user.valid?
    User Exists (0.2ms)  SELECT  1 AS one FROM "users" WHERE LOWER("users"."email") = LOWER(?) LIMIT ?  [["email", "user@example.com"], ["LIMIT", 1]]
  => false
```

2 - What are the error messages for a user with no password?
```
  >> user.errors.full_messages
  => ["Password can't be blank"]
```


# Section 6.3.3

1 - Confirm that a user with valid name and email but a too-short password isn’t valid.
```
  >> user.password = "aaaa"
  => "aaaa"
  >> user.valid?
    User Exists (0.2ms)  SELECT  1 AS one FROM "users" WHERE LOWER("users"."email") = LOWER(?) LIMIT ?  [["email", "user@example.com"], ["LIMIT", 1]]
  => false
```

2 - What are the associated error messages?
```
  >> user.errors.full_messages
  => ["Password confirmation doesn't match Password", "Password is too short (minimum is 6 characters)"]
```

# Section 6.3.4

1 - Quit and restart the console, and then find the user created in this section.
```
  >> user = User.find_by(email: "mhartl@example.com")
    User Load (0.2ms)  SELECT  "users".* FROM "users" WHERE "users"."email" = ? LIMIT ?  [["email", "mhartl@example.com"], ["LIMIT", 1]]
  => #<User id: 1, name: "Michael Hartl", email: "mhartl@example.com", created_at: "2017-04-14 20:26:51", updated_at: "2017-04-14 20:26:51", password_digest: "$2a$10$xVX/aMqkLwrNloB9RjN97O1snHPO.1TXiIXx.Zx34uX...">
```

2 - Try changing the name by assigning a new name and calling save. Why didn’t it work?
```
?
```

3 - Update user’s name to use your name. Hint: The necessary technique is covered in Section 6.1.5.
```
  user.update_attribute(:name, "Nadin")
```


## Capitulo 7

# Section 7.1.1

1 - PERGUNTA
```
  controller: static_pages
  action: about
```

2 - PERGUNTA
```
  >> puts user.attributes.to_yaml
  ---
  id: 1
  name: Nadin
  email: mhartl@example.com
  created_at: !ruby/object:ActiveSupport::TimeWithZone
  utc: &1 2017-04-14 20:26:51.274950000 Z
  zone: &2 !ruby/object:ActiveSupport::TimeZone
    name: Etc/UTC
  time: *1
  updated_at: !ruby/object:ActiveSupport::TimeWithZone
  utc: &3 2017-04-14 20:30:28.074011000 Z
  zone: *2
  time: *3
  password_digest: "$2a$10$xVX/aMqkLwrNloB9RjN97O1snHPO.1TXiIXx.Zx34uXmWxLCBAGDO"
  => nil

  usando y user.attributes faz a mesma coisa
```

# Section 7.1.2

1 - PERGUNTA
```
  <%= @user.created_at %>, <%= @user.updated_at %>
```

2 - PERGUNTA
```
  <%= Time.now %>, quando da refresh a hora atualiza
```

# Section 7.1.3

1 - PERGUNTA
```
(byebug) puts @user.attributes.to_yaml
o site e mais limitado, tem menos informações sobre a hash
```

2 - PERGUNTA
```
  nill
```

# Section 7.1.4

1 - PERGUNTA
```
dee6b5245babfdb7dd16ed5174c35874
```

2 - PERGUNTA
```
  ok funciona
```

3 - PERGUNTA
```
  ok funciona
```

# Section 7.2.1

1 - PERGUNTA
```
  NoMethodError in Users#new undefined method `nome' for #<User:0x007fd7c2ecca60>
```

2 - PERGUNTA
```
  porque pode coincidir com alguma gem
```

# Section 7.2.2

1 - PERGUNTA
```
  ...
```

# Section 7.3.2

1 - PERGUNTA
```
  OK
```

# Section 7.3.3

1 - PERGUNTA
```
  Sim, atualiza automaticamente
```

2 - PERGUNTA
```
  ...
```

# Section 7.3.4

1 - PERGUNTA
```
  ...
```

2 - PERGUNTA
```
  ...
```

3 - PERGUNTA
```
  ...
```

4 - PERGUNTA
```
  ...
```

# Section 7.4.1

1 - PERGUNTA
```
  Foi criado!
```

2 - PERGUNTA
```
  Mesma coisa
```

# Section 7.4.2

1 - PERGUNTA
```
  => "success"
```

2 - PERGUNTA
```
  ...
```

# Section 7.4.3

1 - PERGUNTA
```
  >> User.find_by(email: "asduhfhuasd@asdhufhuas.com")
  User Load (0.3ms)  SELECT  "users".* FROM "users" WHERE "users"."email" = ? LIMIT ?  [["email", "asduhfhuasd@asdhufhuas.com"], ["LIMIT", 1]]
  => #<User id: 1, name: "vininciius", email: "asduhfhuasd@asdhufhuas.com", created_at: "2017-04-24 00:23:13", updated_at: "2017-04-24 00:23:13", password_digest: "$2a$10$sN7OHEnnHjOdHfXDoAx67Okf5BuFFSvrv4iRyobc1Pm...">
```

2 - PERGUNTA
```
  ok, aparece
```

# Section 7.4.4

1 - PERGUNTA
```
  ...
```

2 - PERGUNTA
```
  ...
```

3 - PERGUNTA
```
  ...
```

4 - PERGUNTA
```
  ...
```

## Capitulo 8

# Section 8.1.1

1 - What is the difference between GET login_path and POST login_path?
```
  get envia todas as informacoes pela url, uma enorme brecha na seguranca, alem de haver um retorno ao cliente
  post faz o envio por URI assim nao sendo retornavel, tornando o post mais seguro
```

2 - By piping the results of rails routes to grep, list all the routes associated with the Users resource. Do the same for Sessions. How many routes does each resource have? Hint: Refer to the section on grep in Learn Enough Command Line to Be Dangerous.

```
  Nao consegui fazer o grep
```

# Section 8.1.2

1 - Submissions from the form defined in Listing 8.4 will be routed to the Session controller’s create action. How does Rails know to do this? Hint: Refer to Table 8.1 and the first line of Listing 8.5.
```
   Por causa das rotas, ele procura pela rota /login que utiliza post então ele sabe qual caminho tomar
```

# Section 8.2.1

1 - Log in with a valid user and inspect your browser’s cookies. What is the value of the session content? Hint: If you don’t know how to view your browser’s cookies, Google for it (Box 1.1).

```
 d3RWc0hRaE9HTFQzQmxXNzFJbk8yaDMycUEzb29pZXpETHBaYWZSNXVwSVEvR1Q5Tk56WEE1Q0dneGtwajZRVTRmdU9peDk1anJZbXRnZFZJcWs2UjU0Zk9BRk1tbzUzSUMyckdYYVkyWllyQUVDM1k0MmZ4bjM4Um5IVVlhTVcyMS9Idko2ejlQblFNaXV6M0s0WjFRMllRcHJrOTA4YUUrMVUyY3VqYTNRPS0tUmRZZ1BOSk5SYzNVYTJTaHZXS3g4QT09--0deeeb76a1e2201ded8bcfdda9bf49b78c903815
```

2 - What is the value of the Expires attribute from the previous exercise?

```
   Session
```

# Section 8.2.2

1 - Confirm at the console that User.find_by(id: ...) returns nil when the corresponding user doesn’t exist.

```
 User.find_by(id: 3) Retorna nill
```

2 - In a Rails console, create a session hash with key :user_id. By following the steps in Listing 8.17, confirm that the ||= operator works as required.

```
>> session = {}
>> session[:user_id] = nil
>> @current_user ||= User.find_by(id: session[:user_id])
<What happens here?> => nil
>> session[:user_id]= User.first.id
>> @current_user ||= User.find_by(id: session[:user_id])
<What happens here?> => #<User id: 1, name: "vininciius", email: "asduhfhuasd@asdhufhuas.com", created_at: "2017-04-24 00:23:13", updated_at: "2017-04-24 00:23:13", password_digest: "$2a$10$sN7OHEnnHjOdHfXDoAx67Okf5BuFFSvrv4iRyobc1Pm...">
>> @current_user ||= User.find_by(id: session[:user_id])
<What happens here?> => #<User id: 1, name: "vininciius", email: "asduhfhuasd@asdhufhuas.com", created_at: "2017-04-24 00:23:13", updated_at: "2017-04-24 00:23:13", password_digest: "$2a$10$sN7OHEnnHjOdHfXDoAx67Okf5BuFFSvrv4iRyobc1Pm...">
```

# Section 8.2.3

1 - Confirm at the console that User.find_by(id: ...) returns nil when the corresponding user doesn’t exist.

```
Sim após o refresh da pagina o link foi revertido
```

2 - In a Rails console, create a session hash with key :user_id. By following the steps in Listing 8.17, confirm that the ||= operator works as required.

```
Sim o link foi revertido
```

# Section 8.2.5

1 - Is the test suite red or green if you comment out the log_in line in Listing 8.25?

```
Red
```

2 - By using your text editor’s ability to comment out code, toggle back and forth between commenting out code in Listing 8.25 and confirm that the test suite toggles between red and green. (You will need to save the file between toggles.)

```
  Correto!
```

# Section 8.3

1 - Confirm in a browser that the “Log out” link causes the correct changes in the site layout. What is the correspondence between these changes and the final three steps in Listing 8.31?

```
Total correspondência pois os passos finais, as linhas adicionadas referen-se a destruição do cookie, redirecionamento caso não exista uma ‘sessao’
```

2 - By checking the site cookies, confirm that the session is correctly removed after logging out.

```
Sim, a sessão foi removida e não tem mais nenhum cookie gravado do site
```

## Capitulo 9

# Section 9.1.1

1 - In the console, assign user to the first user in the database, and verify by calling it directly that the remember method works. How do remember_token and remember_digest compare?
```
  When presented with a cookie containing a persistent user id, find the user in the database using the given id, and verify that the remember token cookie matches the associated hash digest from the database.
```

## Capitulo 11

# Section 11.1.1

1 - Verify that the test suite is still green.
```
  Sim...
```

2 - Why does Table 11.2 list the url form of the named route instead of the path form? Hint: We’re going to use it in an email.
```
  Porque o form retorna um path absoluto.
```

# Section 11.1.2

1 - Verify that the test suite is still green after the changes made in this section.
```
  Sim...
```

2 - By instantiating a User object in the console, confirm that calling the create_activation_digest method raises a NoMethodError due to its being a private method. What is the value of the user’s activation digest?
```
  >> user.activation_digest
  => "$2a$10$pNMlV/PaTjrvN01zpA.jlOL35gHt.2HH8Ltz9rKq.GoJNOQrIqL5y"
```

3 - In Listing 6.34, we saw that email downcasing can be written more simply as email.downcase! (without any assignment). Make this change to the downcase_email method in Listing 11.3 and verify by running the test suite that it works.

```
 Ok...
```

# Section 11.2.1

1 - At the console, verify that the escape method in the CGI module escapes out the email address as shown in Listing 11.15. What is the escaped value of the string "Don’t panic!"?
```
>> CGI.escape("Don't panic!")
=> "Don%27t+panic%21"
```

# Section 11.2.2

1 - At the console, verify that the escape method in the CGI module escapes out the email address as shown in Listing 11.15. What is the escaped value of the string "Don’t panic!"?
```
  Procura a data atual
```

# Section 11.2.4

1 - Sign up as a new user and verify that you’re properly redirected. What is the content of the generated email in the server log? What is the value of the activation token?
```
  Foi redirecionado corretamente...
  Token: "$2a$10$xC.0n35idZaEzmzJyLTMa.hm9BP3sHcbxZ2rBuFfm2fGxPiGTzSMC"
```

2 - Verify at the console that the new user has been created but that it is not yet activated.
```
Esta ativado pois eu ativei, mas era pra nao estar...
>> User.last.activated?
User Load (0.3ms)  SELECT  "users".* FROM "users" ORDER BY "users"."id" DESC LIMIT ?  [["LIMIT", 1]]
=> true
```

# Section 11.3.1

1 - Create and remember new user at the console. What are the user’s remember and activation tokens? What are the corresponding digests?
```
>> u.remember_token
=> "B8X99vX494m59mfVdwxaug"
>> u.remember_digest
=> "$2a$10$lFeBJVzpTaGE9A7.PLiJoOUesHG/iUntz.8geYADvCGZpN4zCAnJy"
>>
```

2 - Using the generalized authenticated? method from Listing 11.26, verify that the user is authenticated according to both the remember token and the activation token.
```
>> u.authenticated?(:remember, u.remember_token)
=> true
>> u.activated?
=> false
```

# Section 11.4.1

1 -  1. Sign up for a new account in production. Did you get the email?
```
ok....
```

## Capitulo 12

# Section 12.1.1

1 - Verify that the test suite is still green.
```
  Sim...
```

2 - Why does Table 12.1 list the url form of the edit named route instead of the path form? Hint: The answer is the same as for the similar account activations exercise (Section 11.1.1.1).
```
  Porque o form retorna um path absoluto.
```

# Section 12.1.2
1 - Why does the form_for in Listing 12.4 use :password_reset instead of @password_reset?
```
  Porque @password_reset serve para referenciar ao model e assim contem coisas inutilizaveis no momento...
```

# Section 12.1.3
1 - Submit a valid email address to the form shown in Figure 12.6. What error message do you get?
```
  Erro de logica pois ainda nao foi implementado a funcao para enderecos de email validos...
```

2 - Confirm at the console that the user in the previous exercise has valid reset_digest and reset_sent_at attributes, despite the error. What are the attribute values?
```
  reset_digest: "$2a$10$TCr3T21WjSUruF2kwsrIxuQ2kfptQ.HPir1.bHA5fYy...", reset_sent_at: "2017-05-30 17:33:50">
```

# Section 12.2.1
1 - Preview the email templates in your browser. What do the Date fields read for your previews?
```
  A data atual
```

# Section 12.2.2
1 - Run just the mailer tests. Are they green?
```
  Sim...
```
2 - Confirm that the test goes red if you remove the second call to CGI.escape in Listing 12.12.
```
  Sim...
```

# Section 12.3.1
1 - Follow the link in the email from the server log in Section 12.2.1.1. Does it properly render the form as shown in Figure 12.11?
```
  Sim...
```
2 - What happens if you submit the form from the previous exercise?
```
  Acontece um erro pois tem coisas que nao foram implementadas
```

# Section 12.3.2
1 - Follow the email link from Section 12.2.1.1 again and submit mismatched passwords to the form. What is the error message?
```
  As senhas nao sao 'iguais'
```

# Section 12.4.1

1 - Sign up for a new account in production. Did you get the email?
```
  Falta eu ativar a conta do sendgrid...
```
2 -  Click on the link in the activation email to confirm that it works. What is the corresponding entry in the server log? Hint: Run heroku logs at the command line.
```
  Falta eu ativar a conta do sendgrid...
```
3 - Are you able to successfully update your password?
```
  Falta eu ativar a conta do sendgrid...
```
